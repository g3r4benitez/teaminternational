from builtins import ValueError

from baseclass import BaseClass
from stats import Stats
from config import LAST_NUMBER, FIRST_NUMBER


class DataCapture(BaseClass):
    stat = Stats()
    numbers = []

    def add(self, number):
        __class__.validate_number(number)
        self.numbers.append(number)

    def get_less(self, number: int):
        __class__.validate_number(number)
        if number in self.numbers:
            return self.numbers.index(number)
        else:
            raise ValueError("The number uses for check 'less than' is not present in the set")

    def get_greater(self, number: int):
        __class__.validate_number(number)
        if number in self.numbers:
            return len(self.numbers) - (self.numbers.index(number) + 1)
        else:
            raise ValueError("The number uses for check 'greater than' is not present in the set")


    def build_stats(self):
        self.stat.set_numbers(self.numbers)
        first = min(self.numbers)
        last = max(self.numbers)
        total_items = len(self.numbers)
        qtt_less = 0
        qtt_great = 0

        reverse_index = LAST_NUMBER

        for n in range(FIRST_NUMBER, LAST_NUMBER + 1):
            #less
            if n < first:
                self.stat.set_less_value(n, 0)
            elif n in self.numbers:
                self.stat.set_less_value(n, qtt_less)
                qtt_less += self.numbers.count(n)
            elif n > last:
                self.stat.set_less_value(n, total_items)
            else:
                self.stat.set_less_value(n, qtt_less)

            #greater
            if reverse_index > last:
                self.stat.set_greater_value(reverse_index, 0)
            elif reverse_index in self.numbers:
                self.stat.set_greater_value(reverse_index, qtt_great)
                qtt_great += self.numbers.count(reverse_index)
            elif reverse_index < first:
                self.stat.set_greater_value(reverse_index, total_items)
            else:
                self.stat.set_greater_value(reverse_index, qtt_great)
            reverse_index -= 1

        return self.stat