from baseclass import BaseClass


class Stats(BaseClass):
    __less_total = {}
    __greater_total = {}
    __virtual_index = {}
    __numbers = None

    def get_greater_total(self):
        return self.__greater_total

    def less(self, number: int):
        __class__.validate_number(number)
        return self.__less_total[number]

    def set_less_value(self, index, value):
        self.__less_total[index] = value

    def greater(self, number: int):
        __class__.validate_number(number)
        return self.__greater_total[number]

    def set_greater_value(self, index: int, value: int):
        self.__greater_total[index] = value

    def set_virtual_index(self, index:int, value: int):
        self.__virtual_index[index] = value

    def get_virtual_index(self, index: int):
        return self.__virtual_index[index]

    def set_numbers(self, numbers):
        self.__numbers = numbers

    def between(self, lower: int, higher: int):
        __class__.validate_number(lower)
        __class__.validate_number(higher)
        if lower != higher:
            index_lower = self.less(lower)
            index_higher = self.less(higher)
            modifier = 1 if lower in self.__numbers or higher in self.__numbers else 0
            return index_higher - index_lower + modifier
        else:
            return self.__numbers.count(lower)


