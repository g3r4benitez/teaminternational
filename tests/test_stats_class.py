from team_international.Datacapture import Stats


def test_number_setted_in_less():
    key = 15
    value = 20
    s = Stats()
    s.set_less_value(key, value)
    assert s.less(key) == value


def test_number_setted_in_greater():
    key = 15
    value = 20
    s = Stats()
    s.set_greater_value(key, value)
    assert s.greater(key) == value


def test_number_setted_in_greater():
    key = 15
    value = 20
    s = Stats()
    s.set_greater_value(key, value)
    assert s.greater(key) == value


def test_number_setted_in_virtual_index():
    key = 15
    value = 20
    s = Stats()
    s.set_virtual_index(key, value)
    assert s.get_virtual_index(key) == value


def test_between_differents_index():
    lower = 3
    hight = 6
    s = Stats()
    s.set_virtual_index(lower, 0)
    s.set_virtual_index(hight, 2)
    assert s.between(lower, hight) == 3


def test_between_same_index():
    lower = 3
    hight = 3
    s = Stats()
    s.set_virtual_index(lower, 0)
    s.set_virtual_index(hight, 2)
    assert s.between(lower, hight) == 0
