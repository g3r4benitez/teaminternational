import pytest

from team_international.baseclass import BaseClass


def test_invalid_number_as_string():
    with pytest.raises(ValueError):
        BaseClass.validate_number("uno")


def test_invalid_upper_number():
    with pytest.raises(ValueError):
        BaseClass.validate_number(1001)


def test_invalid_lower_number():
    with pytest.raises(ValueError):
        BaseClass.validate_number(0)




