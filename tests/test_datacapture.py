import pytest

from team_international.datacapture import DataCapture


def test_add_value():
    d = DataCapture()
    d.add(4)
    assert True == (4 in d.numbers)


def test_number_in_index():
    d = DataCapture()
    d.add(4)
    d.add(1)
    d.add(3)
    assert 0 == d.get_less(4)
    assert 1 == d.get_less(1)
    assert 2 == d.get_less(3)


def test_number_get_value_error_in_get_less():
    d = DataCapture()
    d.add(3)
    with pytest.raises(ValueError):
        d.get_less(5)


def test_number_get_value_error_in_get_greater():
    d = DataCapture()
    d.add(3)
    with pytest.raises(ValueError):
        d.get_less(5)


def test_get_greater():
    d = DataCapture()
    d.add(4)
    d.add(1)
    d.add(3)
    d.numbers.sort()
    assert 2 == d.get_greater(1)
    assert 1 == d.get_greater(3)
    assert 0 == d.get_greater(4)
