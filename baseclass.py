from config import LAST_NUMBER, FIRST_NUMBER


class BaseClass:
    @staticmethod
    def validate_number(number: int):
        if type(number) != int:
            raise ValueError("Invalid number")
        if number > LAST_NUMBER:
            raise ValueError("Invalid number, must be under 1000")
        if number < FIRST_NUMBER:
            raise ValueError("Invalid number, Only positive integer")
